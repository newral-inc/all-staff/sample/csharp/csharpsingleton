﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSingleton
{
    public sealed class SingletonClass
    {
        //! 唯一のインスタンス
        private static SingletonClass instance = new SingletonClass();

        /**
         * @brief インスタンスを取得します。
         * 
         * @return インスタンス
         */
        public static SingletonClass GetInstance()
        {
            return instance;
        }

        /**
         * @brief コンストラクタ
         *        外部からインスタンス化させません。
         */
        private SingletonClass() { }
    }
}
